# Solución para CHALLENGE_01.txt
with open(".\\2023\\message_01.txt", "r") as file:
    contenido = file.read()

dic = {}
for palabra in contenido.split():
    palabra = palabra.lower()
    dic[palabra] = dic.get(palabra, 0) + 1

print("La solución es: ")
for elemento in dic:
    print(elemento + str(dic[elemento]), end="")